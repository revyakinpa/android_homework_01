package com.revyakinpa.dz1

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
class MainActivity : AppCompatActivity() {

    companion object {
        const val SECOND_ACTIVITY_RESULT = 0
        const val CONTACT_MODEL_LIST = "contact_model_list"
        private var contactAdapter: ContactAdapter? = null
        private var contactModelArrayList: ArrayList<ContactModel>? = null
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState != null) {
            val saved = savedInstanceState.getParcelableArrayList<ContactModel>(CONTACT_MODEL_LIST)
            if (saved != null){
                createContactList(saved)
            }
        }

        button.setOnClickListener{
            val intent = Intent(baseContext, SecondActivity::class.java)
            startActivityForResult(intent, SECOND_ACTIVITY_RESULT)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SECOND_ACTIVITY_RESULT -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        Toast.makeText(this, "OK", Toast.LENGTH_LONG).show()
                        createContactList(data!!.getParcelableArrayListExtra(ContactIntentService.KEY_OUT))
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(this, "Произошла ошибка", Toast.LENGTH_LONG).show()
                    }
                    else -> {
                        Toast.makeText(this, "unknown error", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }


    private fun createContactList(al: ArrayList<ContactModel>) {
        contactModelArrayList = al
        contactAdapter = ContactAdapter(this, contactModelArrayList!!)
        list_view.adapter = contactAdapter
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState!!.putParcelableArrayList(CONTACT_MODEL_LIST, contactModelArrayList)
    }

}

