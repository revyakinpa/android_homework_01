package com.revyakinpa.dz1

import android.app.IntentService
import android.content.Intent
import android.provider.ContactsContract
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.ListView

class ContactIntentService : IntentService("ContactSerivce") {

    companion object {
        const val ACTION = "com.revyakinpa.dz1.RESPONSE"
        const val KEY_OUT = "KEY_OUT"

        private var contactModelArrayList: ArrayList<ContactModel>? = null
    }
    override fun onHandleIntent(p0: Intent?) {

        contactModelArrayList = ArrayList()

        val phones = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
        while (phones!!.moveToNext()) {
            val name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            val phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

            val contactModel = ContactModel()
            contactModel.setNames(name)
            contactModel.setNumbers(phoneNumber)
            contactModelArrayList!!.add(contactModel)
        }
        phones.close()

        val responseIntent = Intent()
        responseIntent.action = ACTION
        responseIntent.addCategory(Intent.CATEGORY_DEFAULT)
        responseIntent.putParcelableArrayListExtra(KEY_OUT, contactModelArrayList)
        LocalBroadcastManager.getInstance(this).sendBroadcast(responseIntent)

    }

}