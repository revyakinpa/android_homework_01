package com.revyakinpa.dz1

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    //setResult(int)


    companion object {
        private const val CONTACTS_PERMISSION_REQUEST_CODE = 1
        var broadCastReceiver : BroadcastReceiver? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
    }

    override fun onStart() {
        super.onStart()

        val permissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)

        if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
           startService()
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.READ_CONTACTS),
                CONTACTS_PERMISSION_REQUEST_CODE
            )
        }
    }

    fun startService() {
        val intentService = Intent(this, ContactIntentService::class.java)
        startService(intentService)
        broadCastReceiver = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
        val intentFilter = IntentFilter(ContactIntentService.ACTION)
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT)
        val localBroadcastManager = LocalBroadcastManager.getInstance(this)
        localBroadcastManager.registerReceiver(broadCastReceiver as BroadcastReceiver, intentFilter)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CONTACTS_PERMISSION_REQUEST_CODE -> {
                if (grantResults.size > 0 && grantResults[0] === PackageManager.PERMISSION_GRANTED) {
                    startService()
                } else {
                    setResult(Activity.RESULT_CANCELED, intent)
                    finish()
                }
                return
            }
        }
    }

    override fun onDestroy() {
        val localBroadcastManager = LocalBroadcastManager.getInstance(this)
        if (broadCastReceiver != null) {
            localBroadcastManager.unregisterReceiver(broadCastReceiver!!)
        }

        super.onDestroy()
    }


}
